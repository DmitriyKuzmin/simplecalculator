package com.kdp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Calculator
 *
 * @version 1.0
 */
public class Calculator {
    BufferedReader bufferedReader;

    public Calculator(InputStreamReader isr) {
        bufferedReader = new BufferedReader(isr);
    }

    public void run() {
        double operand1 = 0, operand2 = 0, result = 0;
        char operation;

        int state = 0;

        String inputString;


        try {
            while (true) {
                switch (state) {
                    case 0:
                        System.out.print("Operand #1: ");
                        inputString = bufferedReader.readLine();
                        if (inputString.isEmpty())
                            break;
                        operand1 = Double.parseDouble(inputString);
                        state = 1;
                        break;
                    case 1:
                        System.out.print("Operand #2: ");
                        inputString = bufferedReader.readLine();
                        if (inputString.isEmpty())
                            break;
                        operand2 = Double.parseDouble(inputString);
                        state = 2;
                        break;
                    case 2:
                        System.out.print("Operation(+, -, *, /): ");
                        inputString = bufferedReader.readLine();
                        if (inputString.isEmpty())
                            break;
                        operation = inputString.charAt(0);
                        switch (operation) {
                            case '+':
                                result = operand1 + operand2;
                                break;
                            case '-':
                                result = operand1 - operand2;
                                break;
                            case '*':
                                result = operand1 * operand2;
                                break;
                            case '/':
                                result = operand1 / operand2;
                                break;
                        }
                        state = 3;
                        System.out.println("Result: " + result);
                    case 3:
                        System.out.print("1. Continue with this result, 2. Input new operands, 3. Exit: ");
                        inputString = bufferedReader.readLine();
                        if (inputString.isEmpty())
                            break;
                        int next = Integer.parseInt(inputString);

                        switch (next) {
                            case 1:
                                operand1 = result;
                                state = 1;
                                System.out.println("Operand 1: " + operand1);
                                break;
                            case 2:
                            default:
                                state = 0;
                                break;
                            case 3:
                                return;

                        }
                    default:
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Calculator(new InputStreamReader(System.in)).run();
    }
}