# Simple Calculator

Simple calculator for command line.

## Screenshots Demo
Basic order instructions

![](./images/1.png )


Next operation

![](./images/2.png )

Full listing

![](./images/3.png )

## Data ##
Input:

 * operand 1,
 * operand 2,
 * operation

Output:

 * result operation of operands

After this offers to choose next action:

 1. Continue with this result,
 2. Input new operands,
 3. Exit